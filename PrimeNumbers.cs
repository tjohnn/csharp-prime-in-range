
class IsPrime
{
	static void Main()
	{
		System.Console.WriteLine("Enter enter a small number and a large number respectively:");
		string input1 = System.Console.ReadLine();
		string input2 = System.Console.ReadLine();
		
		int number1 = 0;
		bool parse = int.TryParse(input1, out number1);
		
		int number2 = 0;
		parse = int.TryParse(input2, out number2);
		
		System.Diagnostics.Stopwatch myWatch = new System.Diagnostics.Stopwatch();
		myWatch.Start();
		
		//System.Console.WriteLine((int)System.Math.Ceiling((double) System.Math.Pow(number, 0.5) )+"");
		if(parse){
			if(number1 >= number2){
				System.Console.WriteLine("First input must be greater than second input");
			}
			else{
				System.Console.WriteLine("Prime numbers between "+number1+" and "+number2+" are given below");
				System.Console.WriteLine("==========================================================");
				for(int i = number1; i <= number2; i++){
					bool isPrime = true;
					int limit = (int)System.Math.Ceiling((double) System.Math.Pow(i, 0.5) );
					for(int j = 2; j <= limit; j++){
						if(i % j == 0){
							isPrime = false;
							break;
						}
					}
					if(isPrime){
						System.Console.Write(" "+i);
					}
				}
			}
			
		}
		else{
			System.Console.WriteLine("You did not enter a number in one or both of the inputs");
		}
		
		myWatch.Stop();
		System.Console.WriteLine("Time: {0}", myWatch.Elapsed);
	}
}